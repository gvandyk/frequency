package com.vandykweb.frequency;

import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;

public class Frequency {
	private SortedSet<WordCount> sortedWordCountFreq;
	private String bookString;

	public Frequency(String bookString) {
		if (StringUtils.isEmpty(bookString)) {
			throw new IllegalArgumentException("word cannot be null/empty");
		}
		this.bookString = bookString;
		this.sortedWordCountFreq = new TreeSet<WordCount>(
				new FrequencyComparator());

		String splitWords[] = splitBookContents();
		this.sortWordCountFrequency(splitWords);
	}

	private String[] splitBookContents() {
		String[] words = this.bookString.split("\\s+");

		for (int i = 0; i < words.length; i++) {
			words[i] = words[i].replaceAll("[^\\w]", "");
		}
		return words;
	}

	private void sortWordCountFrequency(String[] splitWords) {
		for (String splitWord : splitWords) {
			int count = StringUtils.countMatches(this.bookString, splitWord);
			WordCount wc = new WordCount(splitWord, count);
			this.sortedWordCountFreq.add(wc);
		}
	}

	private WordCount getWordFrequency(String word) throws Exception {
		if (null == word || word.isEmpty()) {
			throw new IllegalArgumentException("word cannot be null/empty");
		}

		for (WordCount wc : this.sortedWordCountFreq) {
			if (wc.compareTo(word) == 0) {
				return wc;
			}
		}
		throw new Exception("Word not found: " + word);
	}

	public static void main(String[] args) {
		Frequency freq = new Frequency("this is a test test to show show show show how this this this this this would work work.");
		WordCount wc;
		try {
			wc = freq.getWordFrequency("show");
			System.out.printf("The word \"%s\" appears %d times.", wc.getWord(), wc.getCount());
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.exit(0);
	}
}
