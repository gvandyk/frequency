package com.vandykweb.frequency;

import java.util.Comparator;

public class FrequencyComparator implements Comparator<WordCount> {

	@Override
	public int compare(WordCount w1, WordCount w2) {
		return (w1.getCount() > w2.getCount()) ? 1 : -1;
	}
}
