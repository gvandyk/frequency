package com.vandykweb.frequency;

import org.apache.commons.lang3.StringUtils;

public class WordCount implements Comparable<String> {
	private String word;
	private int count;

	public WordCount(String word, int count) {
		if (StringUtils.isEmpty(word)) {
			throw new IllegalArgumentException("word cannot be null/empty");
		} else if (count < 0) {
			throw new IllegalArgumentException(
					"word count should not be a negative integer value");
		}
		this.count = count;
		this.word = word;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public int compareTo(String word) {
		return this.getWord().compareTo(word);
	}

	@Override
	public int hashCode() {
		return this.getWord().hashCode();
	}
}
